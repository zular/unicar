$(function(){

    //close menu
    $(document).click(function(event) {
        if ($(event.target).closest(".hidden-menu").length) return;
        $(".hidden-menu").collapse('hide');
    });

    //darkbox init
    if ($('a[rel=darkbox]').length) {
        $('a[rel=darkbox]').darkbox();
    };

    //yandex map init
    if ($('#map').length) {
        var myMap,
            mapCenter = [45.040305, 38.923543];

        ymaps.ready(init);

        function init () {
            myMap = new ymaps.Map('map', {
                center: mapCenter,
                zoom: 16,
                controls: []
            });

            var fullscreenControl = new ymaps.control.FullscreenControl();
            myMap.controls.add(fullscreenControl);

            myMap.controls.add('zoomControl', {position: {right: '10px', top: '47px'}});

            myMap.geoObjects
                .add(new ymaps.Placemark(mapCenter, {
                    balloonContent: ''
                }, {
                    preset: 'islands#icon',
                    iconColor: '#615C62'
                }));
        }
    };

    //slick slider init
    if ($('.slick-slider').length) {
        $('.slick-slider').slick({
            prevArrow: '.slick-slider-left',
            nextArrow: '.slick-slider-right'
        });

        $('.slick-slider-wrap').show();
    };

    //add russina language to validate plugin
    $.extend($.validator.messages, {
        required: "Это поле необходимо заполнить.",
        remote: "Пожалуйста, введите правильное значение.",
        email: "Пожалуйста, введите корректный адрес электронной почты.",
        url: "Пожалуйста, введите корректный URL.",
        date: "Пожалуйста, введите корректную дату.",
        dateISO: "Пожалуйста, введите корректную дату в формате ISO.",
        number: "Пожалуйста, введите число.",
        digits: "Пожалуйста, вводите только цифры.",
        creditcard: "Пожалуйста, введите правильный номер кредитной карты.",
        equalTo: "Пожалуйста, введите такое же значение ещё раз.",
        extension: "Пожалуйста, выберите файл с правильным расширением.",
        maxlength: $.validator.format("Пожалуйста, введите не больше {0} символов."),
        minlength: $.validator.format("Пожалуйста, введите не меньше {0} символов."),
        rangelength: $.validator.format("Пожалуйста, введите значение длиной от {0} до {1} символов."),
        range: $.validator.format("Пожалуйста, введите число от {0} до {1}."),
        max: $.validator.format("Пожалуйста, введите число, меньшее или равное {0}."),
        min: $.validator.format("Пожалуйста, введите число, большее или равное {0}.")
    });

    //validate callback form
    if ($('.callback-form').length) {
        $(".callback-form").validate({
            rules: {
                name: {
                    required: true,
                    minlength: 3
                },
                phone: {
                    required: true,
                    minlength: 5,
                    maxlength: 11,
                    number: true
                }
            },
            success: function  (data, e) {
                $(e).siblings("label").remove();
            }
        });
    };

    $(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });
});
