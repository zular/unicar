/**
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    обьявляем переменные и зависимости
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
*/

var gulp         = require('gulp');
var jade         = require('gulp-jade');

var sass         = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var csso         = require('gulp-csso');
var csscomb      = require('gulp-csscomb');
var cssbeautify  = require('gulp-cssbeautify');
var cmq          = require('gulp-combine-media-queries');

var prettify     = require('gulp-html-prettify');

var coffee       = require('gulp-coffee');
var uglify       = require('gulp-uglify');

var gutil        = require( 'gulp-util' );
var ftp          = require( 'vinyl-ftp' );

var browserSync  = require('browser-sync');
var reload       = browserSync.reload;

gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: "./build/"
        },
        open: false
    });
});

gulp.task('reload', function () {
    browserSync.reload();
});

/**
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    задача для закачки проекта на ftp
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
*/


gulp.task( 'upload', function() {

    var conn = ftp.create( {
        host:     'ftp.vaeum.com',
        user:     'u510625194.test',
        password: 'testtest',
        parallel:  1,
        log:       gutil.log
    } );

    var globs = [
        './build/**'
    ];

    // using base = '.' will transfer everything to /public_html correctly
    // turn off buffering in gulp.src for best performance

    return gulp.src( globs, { base: './build/', buffer: false } )
        // .pipe( conn.newer( '/' ) ) // only upload newer files
        .pipe( conn.dest( '/unicar' ) );
} );

/**
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    задача для компиляции jade
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
*/

gulp.task('jade', function(){
    gulp.src('./jade/!(_)*.jade')
        .pipe(jade())
        .pipe(prettify({indent_char: '  ', indent_size: 2}))
        .on('error', console.log)
        .pipe(gulp.dest('./build/'))
        .on('end', browserSync.reload);
        // .pipe(reload({stream:true}));
});

gulp.task('coffee', function() {
  gulp.src('./coffee/**/*.coffee')
    .pipe(coffee())
    .pipe(uglify())
    .pipe(gulp.dest('./build/js/'))
    .pipe(reload({stream:true}));
});

/**
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    задача для компиляции scss файлов
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
*/

gulp.task('sass', function () {
    gulp.src(['./scss/style.scss'])
        .pipe(sass({
            outputStyle: 'nested',
            errLogToConsole: true
        }))
        .pipe(autoprefixer({
            browsers: ['ie >= 8', 'last 3 versions', '> 2%'],
            cascade: false
        }))
        .pipe(cmq())
        // .pipe(csso(true))
        .pipe(csso())
        .pipe(cssbeautify({
            autosemicolon: true
        }))
        .pipe(csscomb())
        .pipe(gulp.dest('./build/css'))
        .pipe(reload({stream:true}));
});

/**
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    список файлов для наблюдения
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
*/

gulp.task('watch', function () {
    gulp.watch('./scss/**/*.scss', ['sass']);
    gulp.watch('./coffee/**/*.coffee', ['coffee']);
    gulp.watch('./jade/**/*.jade', ['jade']);
});

/**
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    задача по-умолчанию
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
*/

gulp.task('default',
    [
        'watch',
        'sass',
        'jade',
        'coffee',
        'browser-sync'
    ]
);
